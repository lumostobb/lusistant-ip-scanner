package com.lumos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

public class IPRunner implements Runnable {

    private int row;
    private String[] ipTemp;
    private String ip;
    private int timeout = 100;
    private int start_point;
    private int end_point;

    public IPRunner(int row, String ip, int start_point, int end_point){
        this.row = row;
        this.ip = ip;
        this.ipTemp = ip.split("\\.");
        this.start_point = start_point;
        this.end_point = end_point;
    }

    public void run(){
        for(int j = start_point; j < end_point; j++){
            String host = ipTemp[0] + "." + ipTemp[1] + "." + (Integer.parseInt(ipTemp[2]) + row) + "." + j;
            try {
                InetAddress.getByName(host).isReachable(5000);
                System.out.println(ipTemp[0] + "." + ipTemp[1] + "." + (Integer.parseInt(ipTemp[2]) + row) + "." + j);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
