package com.lumos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        String ip = "";
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            ip = socket.getLocalAddress().getHostAddress();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        String[] ipTemp = ip.split("\\.");
        int timeout = 100;

        int total_threads = 32;
        // Requires 256 % total thread == 0 else ip loss will happen.
        int per_thread = 256 / (total_threads / 2);

        Thread[] threads = new Thread[total_threads];

        for (int i = 0 ; i < total_threads ; i++){
            if(i % 2 == 0) {
                threads[i] = new Thread(new IPRunner(i % 2, ip, ((i / 2) * per_thread), ((i / 2) * per_thread) + per_thread));
                threads[i].start();
                //  System.out.print("Start: " + ((i / 2) * per_thread) + " ");
               // System.out.println("End: " + (((i / 2) * per_thread) + per_thread));
            }
            else {
                threads[i] = new Thread(new IPRunner(i % 2, ip, ((i - 1) / 2) * per_thread, (((i - 1) / 2) * per_thread) + per_thread));
                threads[i].start();
            }
            System.out.println(i+1 + ". thread initialized.");
        }

        for(int i=0; i<total_threads; i++) {
            threads[i].join(); // TODO Exception handling
        }
		
        String arduinoMac = "60-01-94-2d-b9-ca";
        String arduinoMac2 = "5c-cf-7f-f2-55-e7";


		
		        try {

            Process proc = Runtime.getRuntime().exec("arp -a ");

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

            String s = null;

            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
                if(s.contains(arduinoMac) || s.contains(arduinoMac2)){
                    String arduinoIPAddress = s.split(" ")[2];
                    System.out.println("Arduino's IP Address: " + arduinoIPAddress);
                }
                // read any errors from the attempted command
                while ((s = stdError.readLine()) != null) {
                    System.err.println(s);
                }
            }}catch (IOException ex) {
              System.err.println(ex);
        }
    }
}
